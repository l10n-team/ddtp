#!/usr/bin/perl
use lib '/srv/ddtp.debian.org/ddts/bin/';
use diagnostics;
use strict;
use ddts_common;
use open ':encoding(UTF-8)';

my $packagefile= shift(@ARGV);
my $distribution= shift(@ARGV);
my $description_en_file= shift(@ARGV);   # Optional

my $description_id;
my $description_tag_id;

use DBI;
use Digest::MD5 qw(md5_hex);

my @DSN = ("DBI:Pg:service=ddtp", "", "");

my $dbh = DBI->connect(@DSN,
    { PrintError => 0,
      RaiseError => 1,
      AutoCommit => 0,
    });

die $DBI::errstr unless $dbh;

sub desc_to_parts ($) {
	my $desc = shift;

	my @parts;
	my $part;
	my @lines=split(/\n/, $desc);

	foreach (@lines) {
		if (not @parts) {
			push @parts,$_;
			$part="";
			next;
		}
		if ($_ ne " .") {
			$part.=$_;
			$part.="\n";
		} else {
			push @parts,$part if ($part ne "");
			$part="";
		}
	}
	push @parts,$part if ($part ne "");

	return @parts;

}

sub get_descriptions_from_db {
	my $descriptions = $dbh->selectall_arrayref("SELECT description_id as id, description_md5 as md5, description from description_tb", { Slice => {} });
	my %result;
	for my $row (@$descriptions) {
		$result{$row->{md5}} = $row;
	}
	return %result;
}

sub insert_into_description_milestone_tb {
	my ($number_of_rows, @values) = @_;

	my $sth = "INSERT INTO description_milestone_tb (description_id, milestone) VALUES ".join(", ", ("(?, ?)") x $number_of_rows)." ON CONFLICT DO NOTHING";
	my $foo = $dbh->prepare($sth);
	$foo->execute(@values);
	$dbh->commit;
}

sub cleanup_milestone_tb {
	my @ids = @_;
	my $size = scalar @ids;

	my $sth = $dbh->prepare("DELETE FROM description_milestone_tb WHERE description_id IN (".join(", ", ("?") x $size).") and 
																				not milestone like 'rtrn:%' and 
																				not milestone like 'popc:%' and 
																				not milestone like 'part:%'");
	$sth->execute(@ids);
	$dbh->commit;
}

sub insert_into_part_description_tb {
	my ($number_of_rows, @values) = @_;

	my $sth = "INSERT INTO part_description_tb (description_id,part_md5) VALUES ".join(", ", ("(?, ?)") x $number_of_rows)." ON CONFLICT DO NOTHING";
	my $foo = $dbh->prepare($sth);
	$foo->execute(@values);
	$dbh->commit;
}

sub insert_into_active_tb {
	my @ids = @_;
	my $size = scalar @ids;

	my $sth = $dbh->prepare("INSERT INTO active_tb (description_id) VALUES ".join(", ", ("(?)") x $size)." ON CONFLICT DO NOTHING");
	$sth->execute(@ids);
	$dbh->commit;
}

sub insert_oldtranslang_into_description_milestone_tb {
	my ($description_id, @oldtranslangs) = @_;
	my $size = scalar @oldtranslangs;

	print "     save_oldtranslang_milestone_to_db: $description_id task: " . join(" ", @oldtranslangs) . " #:$#oldtranslangs\n";

	my $sth = $dbh->prepare("INSERT INTO description_milestone_tb (description_id,milestone) VALUES ".join(", ", ("($description_id, ?)") x $size)." ON CONFLICT DO NOTHING");
	$sth->execute(@oldtranslangs);
	$dbh->commit;
}

sub extract_milestones {
	my ($description_id, $priority, $section, @task) = @_;

	my @values = ();
	my $number_of_rows = 0;

	push (@values, $description_id);
	push (@values, "prio:$priority");
	$number_of_rows++;
	push (@values, $description_id);
	push (@values, "sect:$section");
	$number_of_rows++;
	foreach (@task) {
		push (@values, $description_id);
		push (@values, "task:$_");
	$number_of_rows++;
	}

	return ($number_of_rows, @values);
}

sub extract_tag_milestones {
	my ($description_id, @tags) = @_;

	my @splittags;
	my $number_of_rows = 0;

	foreach my $in (@tags) {
		if ($in =~ m/^([^{]*)\{([^}]*)\}(.*)$/) {
			my $prefix  = $1;
			my $postfix = $3;
			my @list = split /,/,$2;
			foreach my $o (@list) {
				push (@splittags, $description_id);
				push (@splittags, "tags:$prefix.$o.$postfix");
				$number_of_rows++;
			}
		} else {
			push (@splittags, $description_id);
			push (@splittags, "tags:$in");
			$number_of_rows++;
		}
	}

	return ($number_of_rows, @splittags);
}

# Determines whether description changes are insignificant,
# in which case a translation of the previous version of the description can be used.
sub is_minor_changes {
	my ($old, $new) = @_;
	my ($stripped_old, $stripped_new, $indent_old, $indent_new, $text_old, $text_new, @par_sep_old, @par_sep_new);

	$stripped_old = $old =~ s/\s+//gr;
	$stripped_new = $new =~ s/\s+//gr;

	if (lc($stripped_old) ne lc($stripped_new)) { return 0 };

	# compare lists
	# here content means a lowercase version without whitespace characters
	# possible cases:
	# * indentation is the same, content of indented lines is the same — minor changes
	# * indentation is the same, content of indented lines is different — major changes
	#   this is possible, for example, when a line was indented without adding a list
	#   item marker, and an indent of another line without a list item marker was removed
	# * indentation is different, content of indented lines is different — major changes
	# * indentation is different, content of indented lines is the same — major changes
	#   this could mean that the indentation level or line breaks or both were changed,
	#   and it is difficult to determine whether only line breaks were changed or also
	#   the indentation level, so both types of changes are considered significant
	while ($old =~ /^( {2,})(.+)/gm) {
		$indent_old .= "$1*";
		$text_old .= $2 =~ s/\s+//gr;
	}

	while ($new =~ /^( {2,})(.+)/gm) {
		$indent_new .= "$1*";
		$text_new .= $2 =~ s/\s+//gr;
	}

	if (($indent_old ne $indent_new) or (lc($text_old) ne lc($text_new))) { return 0 };

	# compare the number of paragraph separators
	# even if the previous tests are passed,
	# descriptions may have different numbers of paragraphs,
	# so check that too
	@par_sep_old = $old =~ /^ \.$/gm;
	@par_sep_new = $new =~ /^ \.$/gm;

	if (scalar(@par_sep_old) != scalar(@par_sep_new)) { return 0 };

	return 1;
}

# Copies a translation of one description to another.
sub copy_translation {
	my ($old_description_id, $description_id, $old_description, $description) = @_;

	eval {
		$dbh->do("
			INSERT INTO translation_tb (description_id, translation, language)
			SELECT ?, translation, language FROM translation_tb
			WHERE description_id = ?",
			undef, $description_id, $old_description_id);
		# is_minor_changes() compares the number of paragraphs,
		# so it is unnecessary to compare the size of parts_md5 arrays
		my (@old_parts_md5, @new_parts_md5);
		my @parts = desc_to_parts($old_description);
		foreach (@parts) {
			push @old_parts_md5, get_md5_hex($_);
		}
		@parts = desc_to_parts($description);
		foreach (@parts) {
			push @new_parts_md5, get_md5_hex($_);
		}
		foreach (0..$#old_parts_md5) {
			# this check is not enough
			# there is a possibility that a translation of a new part is already
			# in the database, so the SQL statement has "ON CONFLICT DO NOTHING"
			if ($old_parts_md5[$_] ne $new_parts_md5[$_]) {
				$dbh->do("
					INSERT INTO part_tb (part_md5, part, language)
					SELECT ?, part, language FROM part_tb
					WHERE part_md5 = ?
					ON CONFLICT DO NOTHING",
					undef, $new_parts_md5[$_], $old_parts_md5[$_]);
			}
		}
		$dbh->commit;
	};
	if ($@) {
		warn "Packages2db.pl: failed to copy a translation of the description with ID $old_description_id to the description with ID $description_id: $@";
		$dbh->rollback;
	}
}

sub scan_packages_file {
	my ($filename, $distribution, $description_en) = @_;

	my ($package, $prioritize, $description, $descr_md5, $tag, $source, $priority, $section, @task, @tag, $version);
	my $number_of_milestone_rows = 0;
	my @ids_for_milestones = ();
	my @milestone_values = ();
	my $number_of_parts_rows = 0;
	my @parts_values = ();
	my @active_ids = ();

	sub get_old_description {
		my $package = shift;

		my $sth = $dbh->prepare("
			SELECT description_id, description
			FROM package_version_tb JOIN description_tb USING (description_id)
			WHERE package_version_tb.package = ?
			ORDER BY description_id DESC LIMIT 1");
		$sth->execute($package);
		my ($old_description_id, $old_description) = $sth->fetchrow_array;
		return $old_description_id, $old_description;
	}

	sub get_description_tag_id {
		my $description_id= shift(@_);
		my $distribution= shift(@_);

		my $description_tag_id;

		my $sth = $dbh->prepare("SELECT description_tag_id FROM description_tag_tb WHERE description_id=? and tag=?");
		$sth->execute($description_id, $distribution);
		($description_tag_id) = $sth->fetchrow_array;
		return $description_tag_id;
	}

	sub save_version_to_db {
		my $description_id= shift(@_);
		my $version= shift(@_);
		my $package= shift(@_);
		my $source= shift(@_);

		my $package_version_id;

		my $sth = $dbh->prepare("SELECT package_version_id FROM package_version_tb WHERE description_id=? and package=? and version=?");
		$sth->execute($description_id, $package, $version);
		($package_version_id) = $sth->fetchrow_array;

		if (not $package_version_id) {
			eval {
				$dbh->do("INSERT INTO package_version_tb (description_id,package,version,source) VALUES (?,?,?,?);", undef, $description_id, $package, $version, $source);
				$dbh->commit;   # commit the changes if we get this far
			};
			if ($@) {
				warn "Packages2db.pl: failed to INSERT Package '$package', Version '$version' into package_version_tb: $@\n";
				$dbh->rollback; # undo the incomplete changes
			}
		}
	}

	my %descriptions_from_db = get_descriptions_from_db();
	open (PACKAGES, "$filename") or die "open packagefile failed";
	my $in_descr = 0;
	while (<PACKAGES>) {
		if ($_=~/^$/) {
			my $description_orig=$description;
			my ($old_description_id, $old_description, @oldtranslang);
			undef $description_id;
			eval {
				if (!defined($descr_md5)) {
					$descr_md5 = get_md5_hex($description_orig);
				}
				$description_id = $descriptions_from_db{$descr_md5}->{'id'};
				if ($description_id) {
					$description_tag_id=get_description_tag_id($description_id,$distribution);
					if ($description_tag_id) {
						$dbh->do("UPDATE description_tag_tb SET date_end = CURRENT_DATE WHERE description_tag_id=? AND date_end <> CURRENT_DATE;", undef, $description_tag_id);
					} else {
						$dbh->do("INSERT INTO description_tag_tb (description_id, tag, date_begin, date_end) VALUES (?,?,CURRENT_DATE,CURRENT_DATE);", undef, $description_id, $distribution);
					}

					# Fix previously broken descriptions by a bug in import code
					my $description_in_db = $descriptions_from_db{$descr_md5}->{'description'};
					if ($description_in_db ne $description) {
						print "Update description for $package ($description_id, $descr_md5)\n";
						$dbh->do("UPDATE description_tb SET description = ? WHERE description_id = ? AND package = ? AND description <> ?", undef,
								$description, $description_id, $package, $description);
					}

					# Track changes in priority. Here we update the details of the description if one of:
					# - A package with this description comes along with a higher priority (could still be same package)
					# - The current package has a different (possibly lower) priority than before
					$dbh->do("UPDATE description_tb SET prioritize = ?, package = ?, source = ? WHERE description_id = ? AND CASE WHEN package = ? THEN prioritize <> ? ELSE prioritize < ? END", undef,
								$prioritize, $package, $source, $description_id, $package, $prioritize, $prioritize );
				} else {
					($old_description_id, $old_description) = get_old_description($package);
					if ($old_description_id) {
						print "   changed description from $package ($source)\n" ;
                        # search for translations of the old description:
						my $lang;
						my $sth = $dbh->prepare("SELECT language FROM translation_tb where description_id=?");
						$sth->execute($old_description_id);
						while(($lang) = $sth->fetchrow_array) {
							push @oldtranslang,$lang;
						}
					}
					# Can only happen if Packages file refer to md5 not in Translation-en file and we don't already know it.
					if( $descr_md5 ne get_md5_hex($description_orig) ) {
						die "  skipped package $package ($source), description doesn't match md5 ($descr_md5)";
					}
					my $sth = $dbh->prepare("
						WITH t AS (
							INSERT INTO description_tb (description_md5, description, package, source, prioritize)
							VALUES (?,?,?,?,?)
							RETURNING description_id
						)
						INSERT INTO description_tag_tb (description_id, tag, date_begin, date_end)
						SELECT description_id, ?, CURRENT_DATE, CURRENT_DATE FROM t
						RETURNING description_id");
					$sth->execute($descr_md5, $description, $package, $source, $prioritize, $distribution);
					$description_id = $sth->fetch()->[0];
					print "   add new description from $package ($source) with prio $prioritize\n" ;
				}
				$dbh->commit;   # commit the changes if we get this far
			};
			if ($@) {
				warn "Packages2db.pl: Transaction aborted because $@";
				$dbh->rollback; # undo the incomplete changes
			}
			if ($description_id) {
				save_version_to_db($description_id, $version, $package, $source);

				push (@ids_for_milestones, $description_id);
				my ($new_number, @new_values) = extract_milestones($description_id, $priority, $section, @task);
				$number_of_milestone_rows += $new_number;
				push (@milestone_values, @new_values);

				my ($new_number2, @new_values2) = extract_tag_milestones($description_id, @tag);
				$number_of_milestone_rows += $new_number2;
				push (@milestone_values, @new_values2);
				if ($number_of_milestone_rows > 1000) {
					cleanup_milestone_tb(@ids_for_milestones);
					insert_into_description_milestone_tb($number_of_milestone_rows, @milestone_values);
					$number_of_milestone_rows = 0;
					@milestone_values = ();
					@ids_for_milestones = ();
				}
				if ($#oldtranslang >= 0) {
					insert_oldtranslang_into_description_milestone_tb($description_id, @oldtranslang);
					if (is_minor_changes($old_description, $description)) {
						copy_translation($old_description_id, $description_id, $old_description, $description);
					}
				}
			}
			if (($description_id) and ($distribution eq 'sid')) {
				push (@active_ids, $description_id);

				my @parts = desc_to_parts($description);

				foreach (@parts) {
					push (@parts_values, $description_id);
					push (@parts_values, get_md5_hex($_));
					$number_of_parts_rows++;
				}

				if ($number_of_parts_rows > 1000) {
					insert_into_active_tb(@active_ids);
					@active_ids = ();
					insert_into_part_description_tb($number_of_parts_rows, @parts_values);
					$number_of_parts_rows = 0;
					@parts_values = ();
				}
			}

		}
		if (/^Package: ([\w.+-]+)/) { # new item
			$package=$1;
			$source=$1;
			$prioritize=40;
			$version="1";
			undef $descr_md5;
			$prioritize -= 1 if $package =~ /^(linux|kernel)-/i;
			$prioritize -= 1 if $package =~ /^(linux|kernel)-source/i;
			$prioritize -= 2 if $package =~ /^(linux|kernel)-patch/i;
			$prioritize -= 3 if $package =~ /^(linux|kernel)-header/i;
			$prioritize += 3 if $package =~ /^(linux|kernel)-image/i;
			$prioritize -= 5 if $package =~ /^python-/ && $package !~ /-doc$/;
			$prioritize -= 3 if $package =~ /lib/i;
			$prioritize -= 1 if $package =~ /-doc$/i;
			$prioritize -= 6 if $package =~ /-dev$/i;
			$prioritize -= 6 if $package =~ /-dbg$/i;
			$prioritize -= 6 if $package =~ /-dbgsym$/i;
		}
		if (/^Source: ([\w.+-]+)/) { # new item
			$source=$1;
		}
		if (/^Version: ([\w.+:~-]+)/) { # new item
			$version=$1;
		}
		if (/^Tag: (.+)/) { # new item
			@tag=split(',? +',$1);
			$tag=$1;
			$prioritize += 1;
			$prioritize += 2 if $tag =~ /role[^ ]+program/i;
			$prioritize += 1 if $tag =~ /role[^ ]+metapackage/i;
			$prioritize -= 2 if $tag =~ /role[^ ]+devel-lib/i;
			$prioritize -= 2 if $tag =~ /role[^ ]+source/i;
			$prioritize -= 1 if $tag =~ /role[^ ]+shared-lib/i;
			$prioritize -= 1 if $tag =~ /role[^ ]+data/i;
			# In newer packages files this can be split over multiple lines
			# This is a hack to prevent these lines being added to the description
			$in_descr = 0;
		}
		if (/^Priority: (\w+)/) { # new item
			$priority=$1;
			if ($priority  =~ /extra/i ) {
				$prioritize+=0;
			} elsif ($priority  =~ /optional/i ) {
				$prioritize+=5;
			} elsif ($priority  =~ /standard/i ) {
				$prioritize+=10;
			} elsif ($priority  =~ /important/i ) {
				$prioritize+=15;
			} elsif ($priority  =~ /required/i ) {
				$prioritize+=20;
			}
			if ($distribution  =~ /sid/i ) {
				$prioritize+=2;
			}
		}
		if (/^Maintainer: (.*)/) { # new item
		}
		if (/^Task: (.*)/) { # new item
			@task=split('[, ]+',$1);
			$prioritize+=2;
		}
		if (/^Section: (\w+)/) { # new item
			$section="$1";
			if ($section  =~ /devel/i ) {
				$prioritize-=3;
			} elsif ($section  =~ /net/i ) {
				$prioritize-=2;
			} elsif ($section  =~ /oldlibs/i ) {
				$prioritize-=3;
			} elsif ($section  =~ /libs/i ) {
				$prioritize-=3;
			}
		}
		if (/^Description: (.*)/) { # new item
			$description=$1 . "\n";
			# Following lines are part of this
			$in_descr = 1;
		}
		if (/^ / and $in_descr) {
		    # in_descr is because tag can also be multiple lines
			$description.=$_;
		}
		if (/^Description-md5: (.*)/) { # new item
		    my $md5 = $1;
		    $descr_md5 = $md5;
		    if (defined $description_en->{$md5}) {
		        $description = $description_en->{$md5}{'Description-en'} . "\n";
            }
		}
	}
	close PACKAGES or die "packagefile failed";
	if ($number_of_milestone_rows > 0) {
		cleanup_milestone_tb(@ids_for_milestones);
		insert_into_description_milestone_tb($number_of_milestone_rows, @milestone_values);
	}
	if ($number_of_parts_rows > 0) {
		insert_into_active_tb(@active_ids);
		insert_into_part_description_tb($number_of_parts_rows, @parts_values);
	}
}

sub parse_header_format
{
  my $fh = shift;
  my $sub = shift;

  my $lastfield = undef;
  my $hash;
  while(<$fh>)
  {
    chomp;
    if( /^([\w.-]+): (.*)/ )
    {
      $lastfield = $1;
      $hash->{$1} = $2;
    }
    elsif( /^( .*)/ )
    {
      $hash->{$lastfield} .= "\n$_";
    }
    elsif( /^$/ )
    {
      $sub->( $hash );
      $hash = {};
      $lastfield = undef;
    }
  }
}

# Loads the English translation file, indexed by md5
sub load_english_translations
{
        my $filename = shift;
        my $description_en = {};
        
        my $process_descr = sub {
                my $hash = shift;
                if( not exists $hash->{'Description-en'} ) {
                        die "Bad description $hash->{'Description-md5'}";
                }
                $description_en->{$hash->{'Description-md5'}} = $hash;
        };
	open my $fh, "<", "$filename" or die "open description_en file $filename failed";
	parse_header_format($fh, $process_descr);
	return $description_en;
}

my $description_en = {};
if ( $description_en_file ) {
        $description_en = load_english_translations($description_en_file);
}
        
if ( -r $packagefile ) {
	scan_packages_file($packagefile,$distribution,$description_en)
}
