#!/usr/bin/perl -w
use strict;
use LWP::Simple;
use POSIX qw(strftime);
use DBI;
my @DSN = ("DBI:Pg:service=ddtp", "", "");

my $dbh = DBI->connect(@DSN,
    { PrintError => 0,
      RaiseError => 1,
      AutoCommit => 0,
    });

die $DBI::errstr unless $dbh;

my $sth;

#SELECT package_version_tb.description_id,description_tb.source from package_version_tb join description_tb ON package_version_tb.description_id = description_tb.description_id


$sth = $dbh->prepare("SELECT package_version_tb.description_id,description_tb.source from package_version_tb join description_tb ON package_version_tb.description_id = description_tb.description_id");
$sth->execute();
while(my ($desc_id,$source) = $sth->fetchrow_array) {
	eval {
		$dbh->do("Update package_version_tb SET source='$source' WHERE description_id='$desc_id'");
		$dbh->commit;   # commit the changes if we get this far
	};
	if ($@) {
		warn "can't update package_version_tb source='$source' for id='$desc_id': $@\n";
		$dbh->rollback; # undo the incomplete changes
	}
}

