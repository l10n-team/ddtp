#!/bin/bash -e

. "${DDTP_CONFIG_FILE}"

INPORT2DB="../Packages2db.pl"

cd "${DDTP_BASE_DIR}/Packages"

load_distribution ()
{
	distribution=$1
	parts=$2
	for part in $parts
	do
		file="Packages_${distribution}_${part}"
		transfile="Translation-en_${distribution}_${part}"

		echo `date`: ${distribution}/${part}

        if [ -r "$transfile.bz2" ] ; then
            bunzip2 -k -f $transfile.bz2
		elif [ -r "$transfile.xz" ] ; then
			unxz -k -f $transfile.xz
        else
            transfile=""
        fi
		bunzip2 -k -f $file.bz2
		echo `date`: Packages bunzip2

		$INPORT2DB $file $distribution $transfile
		echo `date`: data in db

		rm -f $file $transfile
	done
}

PARTS="main contrib"
DISTS="$DDTP_DISTS_SUPPORTED $DDTP_DISTS_TESTING"
for DIST in $DISTS; do
    load_distribution $DIST "$PARTS"
done

# Clear active before loading sid (which is what counts as active)
psql "${DDTP_PSQL_CONNECTION_STRING}" -c "TRUNCATE active_tb"
psql "${DDTP_PSQL_CONNECTION_STRING}" -c "TRUNCATE part_description_tb"

load_distribution $DDTP_DISTS_UNSTABLE $PARTS

cd "${DDTP_BASE_DIR}"
./scripts/vacuum.sh
